#ifndef DATAFORMATS_AHCALTBRECHIT_H
#define DATAFORMATS_AHCALTBRECHIT_H 1

#include "DataFormats/CaloRecHit/interface/CaloRecHit.h"
#include "SimG4CMS/HGCalTestBeam/interface/AHCalDetId.h"
#include <cstdlib>
#include <vector>
#include "FWCore/Utilities/interface/TypeWithDict.h"

/** \class AHCalTBRecHit
 *
 * \author Linghui Wu
 *
 */

class AHCalTBRecHit : public CaloRecHit
{

public:
     typedef DetId key_type;

     AHCalTBRecHit();

     AHCalTBRecHit(const DetId& id, Float16_t energy, Float16_t time = 0., uint32_t flags = 0, uint32_t aux = 0);

     /// get the id
     AHCalDetId id() const
     {
	  return AHCalDetId(detid());
     };

     void setAHCalCol(int col){ m_AHCalCol = col; }
     void setAHCalRow(int row){ m_AHCalRow = row; }
     void setAHCalLayer(int depth){ m_AHCalLayer = depth; }
     void setAHCalHitX(double x){ m_AHCalHitX = x; }
     void setAHCalHitY(double y){ m_AHCalHitY = y; }
     void setAHCalHitZ(double z){ m_AHCalHitZ = z; }

     int     getAHCalCol(){ return m_AHCalCol; }
     int     getAHCalRow(){ return m_AHCalRow; }
     int     getAHCalLayer(){ return m_AHCalLayer; }
     double  getAHCalHitX(){ return m_AHCalHitX; }
     double  getAHCalHitY(){ return m_AHCalHitY; }
     double  getAHCalHitZ(){ return m_AHCalHitZ; }

     void setNoiseFlag(bool value){ m_noiseFlag = value; }
     bool getNoiseFlag(){ return m_noiseFlag; }

     void setAHCalRawID(int rawid){ m_AHCalRawID = rawid; }
     uint32_t getAHCalRawID(){ return m_AHCalRawID; }

private:
     bool m_noiseFlag;

     uint32_t m_AHCalRawID;

     int m_AHCalCol;
     int m_AHCalRow;
     int m_AHCalLayer;

     double m_AHCalHitX;
     double m_AHCalHitY;
     double m_AHCalHitZ;
};

std::ostream& operator<<(std::ostream& s, const AHCalTBRecHit& hit);

#endif
