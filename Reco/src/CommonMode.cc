#include <HGCal/Reco/interface/CommonMode.h>
#include <HGCal/Geometry/interface/HGCalTBTopology.h>
#include <HGCal/Geometry/interface/HGCalTBGeometryParameters.h>

CommonMode::CommonMode( HGCalElectronicsMap &emap, CommonModeNoiseMethod meth ) 
{
  m_emap = emap;
  m_cmeth = meth;
  m_threshold = 100; //a bit more than 2 MIPs
  m_expectedMaxTS = 3;

  //all memomy allocation done in Init methods
  switch( m_cmeth ){
  case NOCMSUBTRACTION : { InitPerCHIP(); break;}
  case MEDIANPERCHIP : { InitPerCHIP(); break;}
  case MEDIANPERBOARD : { InitPerBoard(); break;}
  case MEDIANPERBOARDWITHTHRESHOLD : { InitPerBoard(); break;}
  }
}

void CommonMode::InitPerCHIP()
{
  p_cmMapHG = new std::vector<float>[HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA * NUMBER_OF_TIME_SAMPLES];
  p_cmMapLG = new std::vector<float>[HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA * NUMBER_OF_TIME_SAMPLES];
  p_cmMapHGHalf = new std::vector<float>[HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA * NUMBER_OF_TIME_SAMPLES];
  p_cmMapLGHalf = new std::vector<float>[HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA * NUMBER_OF_TIME_SAMPLES];
  for ( int iski = 0; iski < HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA; iski++ ) {
    for ( int it = 0; it < NUMBER_OF_TIME_SAMPLES; it++ ) {
      p_cmMapHG[NUMBER_OF_TIME_SAMPLES * iski + it].reserve(32);
      p_cmMapLG[NUMBER_OF_TIME_SAMPLES * iski + it].reserve(32);
      p_cmMapHGHalf[NUMBER_OF_TIME_SAMPLES * iski + it].reserve(32);
      p_cmMapLGHalf[NUMBER_OF_TIME_SAMPLES * iski + it].reserve(32);
    }
  }
}

void CommonMode::InitPerBoard()
{
  p_cmMapHG = new std::vector<float>[HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * NUMBER_OF_TIME_SAMPLES];
  p_cmMapLG = new std::vector<float>[HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * NUMBER_OF_TIME_SAMPLES];
  p_cmMapHGHalf = new std::vector<float>[HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * NUMBER_OF_TIME_SAMPLES];
  p_cmMapLGHalf = new std::vector<float>[HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * NUMBER_OF_TIME_SAMPLES];
  for ( int iboard = 0; iboard < HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD; iboard++ ) {
    for ( int it = 0; it < NUMBER_OF_TIME_SAMPLES; it++ ) {
      p_cmMapHG[NUMBER_OF_TIME_SAMPLES * iboard + it].reserve(128);
      p_cmMapLG[NUMBER_OF_TIME_SAMPLES * iboard + it].reserve(128);
      p_cmMapHGHalf[NUMBER_OF_TIME_SAMPLES * iboard + it].reserve(128);
      p_cmMapLGHalf[NUMBER_OF_TIME_SAMPLES * iboard + it].reserve(128);
    }
  }
}

void CommonMode::ResetPerCHIP()
{
  for ( int iski = 0; iski < HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA; iski++ ) {
    for ( int it = 0; it < NUMBER_OF_TIME_SAMPLES; it++ ) {
      p_cmMapHG[NUMBER_OF_TIME_SAMPLES * iski + it].clear();
      p_cmMapLG[NUMBER_OF_TIME_SAMPLES * iski + it].clear();
      p_cmMapHGHalf[NUMBER_OF_TIME_SAMPLES * iski + it].clear();
      p_cmMapLGHalf[NUMBER_OF_TIME_SAMPLES * iski + it].clear();
    }
  }
}
void CommonMode::ResetPerBoard()
{
  for ( int iboard = 0; iboard < HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD; iboard++ ) {
    for ( int it = 0; it < NUMBER_OF_TIME_SAMPLES; it++ ) {
      p_cmMapHG[NUMBER_OF_TIME_SAMPLES * iboard + it].clear();
      p_cmMapLG[NUMBER_OF_TIME_SAMPLES * iboard + it].clear();
      p_cmMapHGHalf[NUMBER_OF_TIME_SAMPLES * iboard + it].clear();
      p_cmMapLGHalf[NUMBER_OF_TIME_SAMPLES * iboard + it].clear();
    }
  }
}

void CommonMode::Evaluate( edm::Handle<HGCalTBRawHitCollection> hits )
{
  switch( m_cmeth ){
  case NOCMSUBTRACTION : { break;}
  case MEDIANPERCHIP : { ResetPerCHIP(); EvaluateMedianPerChip(hits); break;}
  case MEDIANPERBOARD : { ResetPerBoard(); EvaluateMedianPerBoard(hits); break;}
  case MEDIANPERBOARDWITHTHRESHOLD : { ResetPerBoard(); EvaluateMedianPerBoardWithThr(hits); break;}
  }
}

void CommonMode::EvaluateMedianPerChip( edm::Handle<HGCalTBRawHitCollection> hits )
{
  for ( auto hit : *hits ) {
    HGCalTBElectronicsId eid( m_emap.detId2eid(hit.detid().rawId()) );
    if ( !m_emap.existsEId(eid) ) continue;
    int iski = hit.skiroc();
    for ( size_t it = 0; it < NUMBER_OF_TIME_SAMPLES; it++ ) {
      if ( hit.detid().cellType() == 0 || hit.detid().cellType() == 4 ) {
        p_cmMapHG[NUMBER_OF_TIME_SAMPLES * iski + it].push_back(hit.highGainADC(it));
        p_cmMapLG[NUMBER_OF_TIME_SAMPLES * iski + it].push_back(hit.lowGainADC(it));
      }
      else if ( hit.detid().cellType() != 5 ) {
        p_cmMapHGHalf[NUMBER_OF_TIME_SAMPLES * iski + it].push_back(hit.highGainADC(it));
        p_cmMapLGHalf[NUMBER_OF_TIME_SAMPLES * iski + it].push_back(hit.lowGainADC(it));
      }
    }
  }

  HGCalTBTopology topo;
  for ( int iski = 0; iski < HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD * HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA; iski++ ) {
    for ( int it = 0; it < NUMBER_OF_TIME_SAMPLES; it++ ) {
      int index = NUMBER_OF_TIME_SAMPLES * iski + it;
      std::sort( p_cmMapHG[index].begin(), p_cmMapHG[index].end() );
      std::sort( p_cmMapLG[index].begin(), p_cmMapLG[index].end() );
      std::sort( p_cmMapHGHalf[index].begin(), p_cmMapHGHalf[index].end() );
      std::sort( p_cmMapLGHalf[index].begin(), p_cmMapLGHalf[index].end() );
      if ( m_cmMap.find( iski ) == m_cmMap.end() ) {
        commonModeNoise cm;
        uint16_t size = p_cmMapHG[index].size();
        cm.fullHG[it] = size % 2 == 0 ? p_cmMapHG[index][size / 2 - 1] : p_cmMapHG[index][size / 2] ;
        cm.outerHG[it] = cm.fullHG[it];
        cm.mergedHG[it] = cm.fullHG[it] * 1.5;
        cm.fullLG[it] = size % 2 == 0 ? p_cmMapLG[index][size / 2 - 1] : p_cmMapLG[index][size / 2] ;
        cm.outerLG[it] = cm.fullLG[it];
        cm.mergedLG[it] = cm.fullLG[it] * 1.5;

        size = p_cmMapHGHalf[index].size();
        cm.halfHG[it] = size % 2 == 0 ? p_cmMapHGHalf[index][size / 2 - 1] : p_cmMapHGHalf[index][size / 2] ;
        cm.mouseBiteHG[it] = cm.halfHG[it] * topo.Cell_Area(3) / topo.Cell_Area(2);
        cm.halfLG[it] = size % 2 == 0 ? p_cmMapLGHalf[index][size / 2 - 1] : p_cmMapLGHalf[index][size / 2] ;
        cm.mouseBiteLG[it] = cm.halfLG[it] * topo.Cell_Area(3) / topo.Cell_Area(2);
        std::pair<int, commonModeNoise> p(iski, cm);
        m_cmMap.insert(p);
      }
      else {
        uint16_t size = p_cmMapHG[index].size();
        m_cmMap[iski].fullHG[it] = size % 2 == 0 ? p_cmMapHG[index][size / 2 - 1] : p_cmMapHG[index][size / 2] ;
        m_cmMap[iski].outerHG[it] = m_cmMap[iski].fullHG[it];
        m_cmMap[iski].mergedHG[it] = m_cmMap[iski].fullHG[it] * 1.5;
        m_cmMap[iski].fullLG[it] = size % 2 == 0 ? p_cmMapLG[index][size / 2 - 1] : p_cmMapLG[index][size / 2] ;
        m_cmMap[iski].outerLG[it] = m_cmMap[iski].fullLG[it];
        m_cmMap[iski].mergedLG[it] = m_cmMap[iski].fullLG[it] * 1.5;
        size = p_cmMapHGHalf[index].size();
        m_cmMap[iski].halfHG[it] = size % 2 == 0 ? p_cmMapHGHalf[index][size / 2 - 1] : p_cmMapHGHalf[index][size / 2] ;
        m_cmMap[iski].mouseBiteHG[it] = m_cmMap[iski].halfHG[it] * topo.Cell_Area(3) / topo.Cell_Area(2);
        m_cmMap[iski].halfLG[it] = size % 2 == 0 ? p_cmMapLGHalf[index][size / 2 - 1] : p_cmMapLGHalf[index][size / 2] ;
        m_cmMap[iski].mouseBiteLG[it] = m_cmMap[iski].halfLG[it] * topo.Cell_Area(3) / topo.Cell_Area(2);
      }
    }
  }
}

void CommonMode::EvaluateMedianPerBoard( edm::Handle<HGCalTBRawHitCollection> hits )
{
  for ( auto hit : *hits ) {
    HGCalTBElectronicsId eid( m_emap.detId2eid(hit.detid().rawId()) );
    if ( !m_emap.existsEId(eid) ) continue;
    int iboard = hit.skiroc()/HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA;
    for ( size_t it = 0; it < NUMBER_OF_TIME_SAMPLES; it++ ) {
      if ( hit.detid().cellType() == 0 || hit.detid().cellType() == 4 ) {
        p_cmMapHG[NUMBER_OF_TIME_SAMPLES * iboard + it].push_back(hit.highGainADC(it));
        p_cmMapLG[NUMBER_OF_TIME_SAMPLES * iboard + it].push_back(hit.lowGainADC(it));
      }
      else if ( hit.detid().cellType() != 5 ) {
        p_cmMapHGHalf[NUMBER_OF_TIME_SAMPLES * iboard + it].push_back(hit.highGainADC(it));
        p_cmMapLGHalf[NUMBER_OF_TIME_SAMPLES * iboard + it].push_back(hit.lowGainADC(it));
      }
    }
  }

  HGCalTBTopology topo;
  for ( int iboard = 0; iboard < HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD; iboard++ ) {
    for ( int it = 0; it < NUMBER_OF_TIME_SAMPLES; it++ ) {
      int index = NUMBER_OF_TIME_SAMPLES * iboard + it;
      std::sort( p_cmMapHG[index].begin(), p_cmMapHG[index].end() );
      std::sort( p_cmMapLG[index].begin(), p_cmMapLG[index].end() );
      std::sort( p_cmMapHGHalf[index].begin(), p_cmMapHGHalf[index].end() );
      std::sort( p_cmMapLGHalf[index].begin(), p_cmMapLGHalf[index].end() );
      if ( m_cmMap.find( iboard ) == m_cmMap.end() ) {
        commonModeNoise cm;
        uint16_t size = p_cmMapHG[index].size();
        cm.fullHG[it] = size % 2 == 0 ? p_cmMapHG[index][size / 2 - 1] : p_cmMapHG[index][size / 2] ;
        cm.outerHG[it] = cm.fullHG[it];
        cm.mergedHG[it] = cm.fullHG[it] * 1.5;
        cm.fullLG[it] = size % 2 == 0 ? p_cmMapLG[index][size / 2 - 1] : p_cmMapLG[index][size / 2] ;
        cm.outerLG[it] = cm.fullLG[it];
        cm.mergedLG[it] = cm.fullLG[it] * 1.5;

        size = p_cmMapHGHalf[index].size();
        cm.halfHG[it] = size % 2 == 0 ? p_cmMapHGHalf[index][size / 2 - 1] : p_cmMapHGHalf[index][size / 2] ;
        cm.mouseBiteHG[it] = cm.halfHG[it] * topo.Cell_Area(3) / topo.Cell_Area(2);
        cm.halfLG[it] = size % 2 == 0 ? p_cmMapLGHalf[index][size / 2 - 1] : p_cmMapLGHalf[index][size / 2] ;
        cm.mouseBiteLG[it] = cm.halfLG[it] * topo.Cell_Area(3) / topo.Cell_Area(2);
        std::pair<int, commonModeNoise> p(iboard, cm);
        m_cmMap.insert(p);
      }
      else {
        uint16_t size = p_cmMapHG[index].size();
        m_cmMap[iboard].fullHG[it] = size % 2 == 0 ? p_cmMapHG[index][size / 2 - 1] : p_cmMapHG[index][size / 2] ;
        m_cmMap[iboard].outerHG[it] = m_cmMap[iboard].fullHG[it];
        m_cmMap[iboard].mergedHG[it] = m_cmMap[iboard].fullHG[it] * 1.5;
        m_cmMap[iboard].fullLG[it] = size % 2 == 0 ? p_cmMapLG[index][size / 2 - 1] : p_cmMapLG[index][size / 2] ;
        m_cmMap[iboard].outerLG[it] = m_cmMap[iboard].fullLG[it];
        m_cmMap[iboard].mergedLG[it] = m_cmMap[iboard].fullLG[it] * 1.5;
        size = p_cmMapHGHalf[index].size();
        m_cmMap[iboard].halfHG[it] = size % 2 == 0 ? p_cmMapHGHalf[index][size / 2 - 1] : p_cmMapHGHalf[index][size / 2] ;
        m_cmMap[iboard].mouseBiteHG[it] = m_cmMap[iboard].halfHG[it] * topo.Cell_Area(3) / topo.Cell_Area(2);
        m_cmMap[iboard].halfLG[it] = size % 2 == 0 ? p_cmMapLGHalf[index][size / 2 - 1] : p_cmMapLGHalf[index][size / 2] ;
        m_cmMap[iboard].mouseBiteLG[it] = m_cmMap[iboard].halfLG[it] * topo.Cell_Area(3) / topo.Cell_Area(2);
      }
    }
  }
}

void CommonMode::EvaluateAveragePerChip( edm::Handle<HGCalTBRawHitCollection> hits )
{
}

void CommonMode::EvaluateMedianPerBoardWithThr( edm::Handle<HGCalTBRawHitCollection> hits )
{
  for ( auto hit : *hits ) {
    HGCalTBElectronicsId eid( m_emap.detId2eid(hit.detid().rawId()) );
    if ( !m_emap.existsEId(eid) ) continue;
    int iboard = hit.skiroc()/HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA;
    if ( hit.highGainADC(m_expectedMaxTS)>m_threshold ) continue;
    for ( size_t it = 0; it < NUMBER_OF_TIME_SAMPLES; it++ ) {
      p_cmMapHG[NUMBER_OF_TIME_SAMPLES * iboard + it].push_back(hit.highGainADC(it));
      p_cmMapLG[NUMBER_OF_TIME_SAMPLES * iboard + it].push_back(hit.lowGainADC(it));
    }
  }
  
  HGCalTBTopology topo;
  for ( int iboard = 0; iboard < HGCAL_TB_GEOMETRY::NUMBER_OF_HEXABOARD; iboard++ ) {
    for ( int it = 0; it < NUMBER_OF_TIME_SAMPLES; it++ ) {
      int index = NUMBER_OF_TIME_SAMPLES * iboard + it;
      std::sort( p_cmMapHG[index].begin(), p_cmMapHG[index].end() );
      std::sort( p_cmMapLG[index].begin(), p_cmMapLG[index].end() );
      if ( m_cmMap.find( iboard ) == m_cmMap.end() ) {
        commonModeNoise cm;
        uint16_t size = p_cmMapHG[index].size();
	if( size>1 ){
	  cm.fullHG[it] = size % 2 == 0 ? p_cmMapHG[index][size / 2 - 1] : p_cmMapHG[index][size / 2] ;
	  cm.outerHG[it] = cm.fullHG[it];
	  cm.mergedHG[it] = cm.fullHG[it] * 1.5;
	  cm.fullLG[it] = size % 2 == 0 ? p_cmMapLG[index][size / 2 - 1] : p_cmMapLG[index][size / 2] ;
	  cm.outerLG[it] = cm.fullLG[it];
	  cm.mergedLG[it] = cm.fullLG[it] * 1.5;
	  cm.halfHG[it] = cm.fullHG[it] * topo.Cell_Area(2) / topo.Cell_Area(0);
	  cm.mouseBiteHG[it] = cm.fullHG[it] * topo.Cell_Area(3) / topo.Cell_Area(0);
	  cm.halfLG[it] = cm.fullLG[it] * topo.Cell_Area(2) / topo.Cell_Area(0);
	  cm.mouseBiteLG[it] = cm.fullLG[it] * topo.Cell_Area(3) / topo.Cell_Area(0);
	}
	else{
	  cm.fullHG[it] = 0;
	  cm.outerHG[it] = 0;
	  cm.mergedHG[it] = 0;
	  cm.fullLG[it] = 0;
	  cm.outerLG[it] = 0;
	  cm.mergedLG[it] = 0;
	  cm.halfHG[it] = 0;
	  cm.mouseBiteHG[it] = 0;
	  cm.halfLG[it] = 0;
	  cm.mouseBiteLG[it] = 0;
	}
        std::pair<int, commonModeNoise> p(iboard, cm);
        m_cmMap.insert(p);
      }
      else {
        uint16_t size = p_cmMapHG[index].size();
	if( size!= 0 ){
	  m_cmMap[iboard].fullHG[it] = size % 2 == 0 ? p_cmMapHG[index][size / 2 - 1] : p_cmMapHG[index][size / 2] ;
	  m_cmMap[iboard].outerHG[it] = m_cmMap[iboard].fullHG[it];
	  m_cmMap[iboard].mergedHG[it] = m_cmMap[iboard].fullHG[it] * 1.5;
	  m_cmMap[iboard].fullLG[it] = size % 2 == 0 ? p_cmMapLG[index][size / 2 - 1] : p_cmMapLG[index][size / 2] ;
	  m_cmMap[iboard].outerLG[it] = m_cmMap[iboard].fullLG[it];
	  m_cmMap[iboard].mergedLG[it] = m_cmMap[iboard].fullLG[it] * 1.5;
	  m_cmMap[iboard].halfHG[it] = m_cmMap[iboard].fullHG[it] * topo.Cell_Area(2) / topo.Cell_Area(0);
	  m_cmMap[iboard].mouseBiteHG[it] = m_cmMap[iboard].fullHG[it] * topo.Cell_Area(3) / topo.Cell_Area(0);
	  m_cmMap[iboard].halfLG[it] = m_cmMap[iboard].fullLG[it] * topo.Cell_Area(2) / topo.Cell_Area(0);
	  m_cmMap[iboard].mouseBiteLG[it] = m_cmMap[iboard].fullLG[it] * topo.Cell_Area(3) / topo.Cell_Area(0);
	}
	else{
	  m_cmMap[iboard].fullHG[it] = 0;
	  m_cmMap[iboard].outerHG[it] = 0;
	  m_cmMap[iboard].mergedHG[it] = 0;
	  m_cmMap[iboard].fullLG[it] = 0;
	  m_cmMap[iboard].outerLG[it] = 0;
	  m_cmMap[iboard].mergedLG[it] = 0;
	  m_cmMap[iboard].halfHG[it] = 0;
	  m_cmMap[iboard].mouseBiteHG[it] = 0;
	  m_cmMap[iboard].halfLG[it] = 0;
	  m_cmMap[iboard].mouseBiteLG[it] = 0;
	}
      }
    }
  }
}
