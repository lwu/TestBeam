#include "HGCal/CondObjects/interface/HGCalCondObjectTextIO.h"
#include <HGCal/CondObjects/interface/HGCalTBTOAConversionsMap_perChannel.h>
#include <algorithm>

ASIC_TOA_Conversions_perChannel HGCalTBTOAConversionsMap_perChannel::getASICConversions(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  return (*std::find(_vec.begin(),_vec.end(),adcConv));
}

uint32_t HGCalTBTOAConversionsMap_perChannel::toa_min(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=std::find(_vec.begin(),_vec.end(),adcConv);
  return it!=_vec.end() ? (*it).toa_min() : 4;
}

uint32_t HGCalTBTOAConversionsMap_perChannel::toa_max(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=std::find(_vec.begin(),_vec.end(),adcConv);
  return it!=_vec.end() ? (*it).toa_max() : 4;
}

float HGCalTBTOAConversionsMap_perChannel::toa_func_a(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=std::find(_vec.begin(),_vec.end(),adcConv);
  return it!=_vec.end() ? (*it).toa_func_a() : 4;
}

float HGCalTBTOAConversionsMap_perChannel::toa_func_b(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=std::find(_vec.begin(),_vec.end(),adcConv);
  return it!=_vec.end() ? (*it).toa_func_b() : 4;
}



float HGCalTBTOAConversionsMap_perChannel::tw_func_a(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=std::find(_vec.begin(),_vec.end(),adcConv);
  return it!=_vec.end() ? (*it).tw_func_a() : 4;
}

float HGCalTBTOAConversionsMap_perChannel::tw_func_b(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=std::find(_vec.begin(),_vec.end(),adcConv);
  return it!=_vec.end() ? (*it).tw_func_b() : 4;
}

float HGCalTBTOAConversionsMap_perChannel::tw_func_c(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=std::find(_vec.begin(),_vec.end(),adcConv);
  return it!=_vec.end() ? (*it).tw_func_c() : 4;
}

float HGCalTBTOAConversionsMap_perChannel::tw_func_d(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=std::find(_vec.begin(),_vec.end(),adcConv);
  return it!=_vec.end() ? (*it).tw_func_d() : 4;
}

float HGCalTBTOAConversionsMap_perChannel::t0_offset(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=std::find(_vec.begin(),_vec.end(),adcConv);
  return it!=_vec.end() ? (*it).t0_offset() : 4;
}

short HGCalTBTOAConversionsMap_perChannel::toa_flag(uint32_t moduleId, uint32_t asicId, uint32_t channelId)
{
  ASIC_TOA_Conversions_perChannel adcConv(moduleId,asicId, channelId);
  std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=std::find(_vec.begin(),_vec.end(),adcConv);
  return it!=_vec.end() ? (*it).toa_flag() : 4;
}

std::ostream& operator<<(std::ostream& s, ASIC_TOA_Conversions_perChannel& adc){
  s << adc.moduleId() << " "
    << adc.asicId() << " "
    << adc.channelId() << " "
    << adc.toa_min() << " "
    << adc.toa_max() << " "
    << adc.toa_func_a() << " "
    << adc.toa_func_b() << " "
    << adc.tw_func_a() << " "
    << adc.tw_func_b() << " "
    << adc.tw_func_c() << " "
    << adc.tw_func_d() << " "
    << adc.t0_offset() << " "
    << adc.toa_flag() << " ";
  return s;
}


std::ostream& operator<<(std::ostream& s, HGCalTBTOAConversionsMap_perChannel& a_map)
{
  s << "moduleId "
    << "asicId "
    << "channelId "
    << "toa_min "
    << "toa_max "
    << "toa_func_a "
    << "toa_func_b "
    << "tw_func_a "
    << "tw_func_b "
    << "tw_func_c "
    << "tw_func_d "
    << "t0_offset "  
    << "toa_flag ";  
  for( std::vector<ASIC_TOA_Conversions_perChannel>::iterator it=a_map.getEntries().begin(); it!=a_map.getEntries().end(); ++it )
    s << (*it) ;
  return s;
}
