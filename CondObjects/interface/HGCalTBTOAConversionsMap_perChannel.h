#ifndef HGCAL_TB_TOT_CONVERSION_MAP_PERCHANNEL_H
#define HGCAL_TB_TOT_CONVERSION_MAP_PERCHANNEL_H

#include <iostream>
#include <vector>

class ASIC_TOA_Conversions_perChannel{
 public:
  ASIC_TOA_Conversions_perChannel(){;}
  ASIC_TOA_Conversions_perChannel(uint32_t moduleId,uint32_t asicId,uint32_t channelId)
    {
      _moduleId=moduleId;
      _asicId=asicId;
      _channelId=channelId;
    }
  ASIC_TOA_Conversions_perChannel(uint32_t moduleId,uint32_t asicId,uint32_t channelId,
		       uint32_t toa_min, uint32_t toa_max,
           float toa_func_a, float toa_func_b,
           float tw_func_a, float tw_func_b, float tw_func_c, float tw_func_d,
           float t0_offset, 
           short toa_flag)
    {
      _moduleId=moduleId;
      _asicId=asicId;
      _channelId=channelId;
      _toa_min=toa_min;
      _toa_max=toa_max;
      _toa_func_a=toa_func_a;
      _toa_func_b=toa_func_b;
      _tw_func_a=tw_func_a;
      _tw_func_b=tw_func_b;
      _tw_func_c=tw_func_c;
      _tw_func_d=tw_func_d;      
      _t0_offset=t0_offset;      
      _toa_flag = toa_flag;
    }
  ~ASIC_TOA_Conversions_perChannel(){;}
  uint32_t moduleId() const {return _moduleId;}
  uint32_t asicId() const {return _asicId;}
  uint32_t channelId() const {return _channelId;}
  uint32_t toa_min() const {return _toa_min;}
  uint32_t toa_max() const {return _toa_max;}
  float toa_func_a() const {return _toa_func_a;}
  float toa_func_b() const {return _toa_func_b;}
  float tw_func_a() const {return _tw_func_a;}
  float tw_func_b() const {return _tw_func_b;}
  float tw_func_c() const {return _tw_func_c;}
  float tw_func_d() const {return _tw_func_d;}
  float t0_offset() const {return _t0_offset;}
  short toa_flag() const {return _toa_flag;}
  
 private:
  uint32_t _moduleId;
  uint32_t _asicId;
  uint32_t _channelId;
  uint32_t _toa_min;
  uint32_t _toa_max;
  float _toa_func_a;
  float _toa_func_b;
  float _tw_func_a;
  float _tw_func_b;
  float _tw_func_c;
  float _tw_func_d;
  float _t0_offset;
  short _toa_flag;
};


std::ostream& operator<<(std::ostream&, ASIC_TOA_Conversions_perChannel&);

inline bool operator==(const ASIC_TOA_Conversions_perChannel& rhs0, const ASIC_TOA_Conversions_perChannel& rhs1) { 
  return rhs0.moduleId() == rhs1.moduleId() && rhs0.asicId() == rhs1.asicId() && rhs0.channelId() == rhs1.channelId();
} 

class HGCalTBTOAConversionsMap_perChannel{
 public:
  HGCalTBTOAConversionsMap_perChannel(){;}
  ~HGCalTBTOAConversionsMap_perChannel(){;}
    
  void addEntry( ASIC_TOA_Conversions_perChannel conv){ _vec.push_back(conv); }
  std::vector<ASIC_TOA_Conversions_perChannel> &getEntries(){ return _vec; }
  ASIC_TOA_Conversions_perChannel getASICConversions(uint32_t moduleId, uint32_t asicId, uint32_t channelId);
  uint32_t toa_min(uint32_t moduleId, uint32_t asicId, uint32_t channelId);
  uint32_t toa_max(uint32_t moduleId, uint32_t asicId, uint32_t channelId);
  float toa_func_a(uint32_t moduleId, uint32_t asicId, uint32_t channelId);
  float toa_func_b(uint32_t moduleId, uint32_t asicId, uint32_t channelId);
  float tw_func_a(uint32_t moduleId, uint32_t asicId, uint32_t channelId);
  float tw_func_b(uint32_t moduleId, uint32_t asicId, uint32_t channelId);
  float tw_func_c(uint32_t moduleId, uint32_t asicId, uint32_t channelId);
  float tw_func_d(uint32_t moduleId, uint32_t asicId, uint32_t channelId);
  float t0_offset(uint32_t moduleId, uint32_t asicId, uint32_t channelId);
  short toa_flag(uint32_t moduleId, uint32_t asicId, uint32_t channelId);

 private:
  std::vector<ASIC_TOA_Conversions_perChannel> _vec;
};

std::ostream& operator<<(std::ostream&, HGCalTBTOAConversionsMap_perChannel&);

#endif
